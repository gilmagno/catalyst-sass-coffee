use strict;
use warnings;
use CatSassCoffee;
use Plack::Builder;

my $app = CatSassCoffee->apply_default_middlewares(CatSassCoffee->psgi_app);

builder {
    enable 'Compile' => (
        pattern => qr{\.coffee$},
        lib     => 'root',
        mime    => 'text/plain',
        map     => sub {
            my $filename = shift;
            $filename =~ s/coffee$/js/;
            return $filename;
        },
        compile => sub {
            my ($in, $out) = @_;
            system("coffee --compile --stdio < $in > $out");
        },
    );

    enable 'Compile' => (
        pattern => qr{\.scss$},
        lib     => 'root',
        mime    => 'text/css',
        map     => sub {
            my $filename = shift;
            $filename =~ s/scss$/css/;
            return $filename;
        },
        compile => sub {
            my ($in, $out) = @_;
            system("sass $in $out");
        },
    );

    $app;
}
